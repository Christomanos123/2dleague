 local player2 = {}
function player2:load()
  player2.body = love.physics.newBody(world, 1600, 995, "dynamic")
  player2.size = love.physics.newRectangleShape(100, 35)
  player2.fixture = love.physics.newFixture(player2.body, player2.size)
  player2.fixture:setUserData("player")
    
	player2.speed = 500
  player2.shots = {} -- holds our fired shots
end

function player2.draw()
 -- let's draw our player2
  love.graphics.setColor(255,0,0,255)
	love.graphics.polygon("fill", player2.body:getWorldPoints(player2.size:getPoints()))
end

return player2