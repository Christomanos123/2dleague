 ground = {}
function ground.load()
  ground.body = love.physics.newBody(world, 0, 1035, "static")
  ground.size = love.physics.newRectangleShape(3840, 150)
  ground.fixture = love.physics.newFixture(ground.body, ground.size)
  ground.fixture:setUserData("ground")  
end

function ground.draw()
  -- let's draw some ground
	love.graphics.setColor(0,200,0,255)
	love.graphics.polygon("fill", ground.body:getWorldPoints(ground.size:getPoints()))
  end

  return ground