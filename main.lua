--The making of 2D rocket league. We have got two cars, two goals, a ball and physics put into this football-like game.
--https://love2d.org/forums/viewtopic.php?f=4&t=84675
-- http://tylerneylon.com/a/learn-lua/
-- https://love2d.org/forums/viewtopic.php?f=4&t=84668&p=217069&hilit=physics#p217069
local ground = require('ground')
local player1 = require('player1')
local ball = require ('ball')
local player2 = require('player2')
local outLine = require('outLine')
function love.load()
	bg = love.graphics.newImage("bg.png")
	world = love.physics.newWorld(0, 200, true)  --Gravity is being set to 0 in the x direction and 200 in the y direction.
  world:setCallbacks(beginContact, endContact, preSolve, postSolve)
  ground.load()  
  player1.load()
  player2.load()
  ball.load()
  outLine.load()
end

function love.update(dt)
  world:update(dt)
  world:update(dt)
  controlP1(dt)
  controlP2(dt)
  
	local remEnemy = {}
	local remShot = {}
end		

function controlP1(dt)
  if love.keyboard.isDown("a") then
		player1.body:applyForce(-5500, 0)
	elseif love.keyboard.isDown("d") then
		player1.body:applyForce(5500, 0)
  end  
end

function controlP2(dt)
  if love.keyboard.isDown("left") then
		player2.body:applyForce(-5500, 0)
	elseif love.keyboard.isDown("right") then
		player2.body:applyForce(5500, 0)
  end
end

function love.keypressed(key)
  if key == "up" then
    player2.body:applyForce(0, -16000)
  elseif key == "down" then
    player2.body:applyForce(0, 12000)
  end 
  
  if key == "q" then
    love.event.quit()
    end
  
  if key == "w" then
    player1.body:applyForce(0, -16000)
  elseif key == "s" then
    player1.body:applyForce(0, 12000)
  end
end

function love.draw()
	-- let's draw a background
	love.graphics.setColor(255,255,255,255)
	love.graphics.draw(bg)
  ground.draw()
  player1.draw()
  player2.draw()
  ball.draw()
  outLine.draw()
end

function shoot()
	
	local shot = {}
	shot.x = player1.x+player1.width/2
	shot.y = player1.y
	
	table.insert(player1.shots, shot)
	
end


function preSolve(a, b, coll)

   -- persisting = persisting + 1    -- keep track of how many updates they've been touching for
end
 
function postSolve(a, b, coll, normalimpulse, tangentimpulse)
end


function CheckCollision(ax1,ay1,aw,ah, bx1,by1,bw,bh)

  local ax2,ay2,bx2,by2 = ax1 + aw, ay1 + ah, bx1 + bw, by1 + bh
  return ax1 < bx2 and ax2 > bx1 and ay1 < by2 and ay2 > by1
end