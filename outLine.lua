outLine = {}

function outLine.load()
  goalPostL = {}
  goalPostL.body = love.physics.newBody(world, 65.05, 400, "static")
  goalPostL.size = love.physics.newRectangleShape(15, 515)
  goalPostL.fixture = love.physics.newFixture(goalPostL.body, goalPostL.size)
  goalPostL.fixture:setUserData("goalpost")  
  
  goalPostR = {}
  goalPostR.body = love.physics.newBody(world, 1855, 400, "static")
  goalPostR.size = love.physics.newRectangleShape(15, 515)
  goalPostR.fixture = love.physics.newFixture(goalPostR.body, goalPostR.size)
  goalPostR.fixture:setUserData("goalpost")
  
  crossBarL = {}
  crossBarL.body = love.physics.newBody(world, 38, 650, "static")
  crossBarL.size = love.physics.newRectangleShape(70, 15)
  crossBarL.fixture = love.physics.newFixture(crossBarL.body, crossBarL.size)
  crossBarL.fixture:setUserData("goalpost")
  
  crossBarR = {}
  crossBarR.body = love.physics.newBody(world, 1882, 650, "static")
  crossBarR.size = love.physics.newRectangleShape(70, 15)
  crossBarR.fixture = love.physics.newFixture(crossBarR.body, crossBarR.size)
  crossBarR.fixture:setUserData("goalpost")
  
  roof = {}
  roof.body = love.physics.newBody(world, 953, 150, "static")
  roof.size = love.physics.newRectangleShape(1790, 15)
  roof.fixture = love.physics.newFixture(roof.body, roof.size)
  roof.fixture:setUserData("goalpost")
  
  goalBZL = {}
  goalBZL.body = love.physics.newBody(world, 5, 915, "static")
  goalBZL.size = love.physics.newRectangleShape(15, 515)
  goalBZL.fixture = love.physics.newFixture(goalBZL.body, goalBZL.size)
  goalBZL.fixture:setUserData("blastzone") 
  
  goalBZR = {}
  goalBZR.body = love.physics.newBody(world, 1915, 915, "static")
  goalBZR.size = love.physics.newRectangleShape(15, 515)
  goalBZR.fixture = love.physics.newFixture(goalBZR.body, goalBZR.size)
  goalBZR.fixture:setUserData("blastzone") 
end

function outLine.draw()
  	  -- let's draw the blastzone (leftside)
  love.graphics.setColor(0,255,0,255)
  love.graphics.polygon("fill", goalBZL.body:getWorldPoints(goalBZL.size:getPoints()))
  
  -- let's draw the blastzone (rightside)
  love.graphics.setColor(0,255,0,255)
  love.graphics.polygon("fill", goalBZR.body:getWorldPoints(goalBZR.size:getPoints()))
  
  
    -- let's draw the roof
  love.graphics.setColor(255,255,255,255)
  love.graphics.polygon("fill", roof.body:getWorldPoints(roof.size:getPoints()))
	
  -- let's draw the cross bar (leftSide)
  love.graphics.setColor(255,255,255,255)
  love.graphics.polygon("fill", crossBarL.body:getWorldPoints(crossBarL.size:getPoints()))
  
    -- let's draw the cross bar (RightSide)
  love.graphics.setColor(255,255,255,255)
  love.graphics.polygon("fill", crossBarR.body:getWorldPoints(crossBarR.size:getPoints()))
  
  -- let's draw the goalpost (leftSide)
  love.graphics.setColor(255,255,255,255)
  love.graphics.polygon("fill", goalPostL.body:getWorldPoints(goalPostL.size:getPoints()))
  
  -- let's draw the goalpost (RightSide)
  love.graphics.setColor(255,255,255,255)
  love.graphics.polygon("fill", goalPostR.body:getWorldPoints(goalPostR.size:getPoints()))
end

return outLine