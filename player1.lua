player1 = {}
function player1.load()
  player1.body = love.physics.newBody(world, 300, 995, "dynamic")
  player1.body:setMass(30)
  player1.size = love.physics.newRectangleShape(100, 35)
  player1.fixture = love.physics.newFixture(player1.body, player1.size)
  player1.fixture:setUserData("player")
  player1.shots = {} -- holds our fired shots
end
function player1.draw()
	-- let's draw our player1
	love.graphics.setColor(0,0,255,255)
  love.graphics.polygon("fill", player1.body:getWorldPoints(player1.size:getPoints()))
    
end
return player1