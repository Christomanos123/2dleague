staticObjects = {}
function staticObjects.load()
--ground = {}
  staticObjects.ground.body = love.physics.newBody(world, 0, 1035, "static")
  staticObjects.ground.size = love.physics.newRectangleShape(3840, 150)
  staticObjects.ground.fixture = love.physics.newFixture(ground.body, ground.size)
  staticObjects.ground.fixture:setUserData("ground")  
  
  --goalPostL = {}
  staticObjects.goalPostL.body = love.physics.newBody(world, 65.05, 400, "static")
  staticObjects.goalPostL.size = love.physics.newRectangleShape(15, 515)
  staticObjects.goalPostL.fixture = love.physics.newFixture(goalPostL.body, goalPostL.size)
  staticObjects.goalPostL.fixture:setUserData("goalpost")  
  
  --goalPostR = {}
  staticObjects.goalPostR.body = love.physics.newBody(world, 1855, 400, "static")
  staticObjects.goalPostR.size = love.physics.newRectangleShape(15, 515)
  staticObjects.goalPostR.fixture = love.physics.newFixture(goalPostR.body, goalPostR.size)
  staticObjects.goalPostR.fixture:setUserData("goalpost")
  
  --crossBarL = {}
  staticObjects.crossBarL.body = love.physics.newBody(world, 38, 650, "static")
  staticObjects.crossBarL.size = love.physics.newRectangleShape(70, 15)
  staticObjects.crossBarL.fixture = love.physics.newFixture(crossBarL.body, crossBarL.size)
  staticObjects.crossBarL.fixture:setUserData("goalpost")
  
  --crossBarR = {}
  staticObjects.crossBarR.body = love.physics.newBody(world, 1882, 650, "static")
  staticObjects.crossBarR.size = love.physics.newRectangleShape(70, 15)
  staticObjects.crossBarR.fixture = love.physics.newFixture(crossBarR.body, crossBarR.size)
  staticObjects.crossBarR.fixture:setUserData("goalpost")
  
 -- roof = {}
  staticObjects.roof.body = love.physics.newBody(world, 953, 150, "static")
  staticObjects.roof.size = love.physics.newRectangleShape(1790, 15)
  staticObjects.roof.fixture = love.physics.newFixture(roof.body, roof.size)
  staticObjects.roof.fixture:setUserData("goalpost")
  
  --goalBZL = {}
  staticObjects.goalBZL.body = love.physics.newBody(world, 5, 915, "static")
  staticObjects.goalBZL.size = love.physics.newRectangleShape(15, 515)
  staticObjects.goalBZL.fixture = love.physics.newFixture(goalBZL.body, goalBZL.size)
  staticObjects.goalBZL.fixture:setUserData("blastzone") 
  
  --goalBZR = {}
  staticObjects.goalBZR.body = love.physics.newBody(world, 1915, 915, "static")
  staticObjects.goalBZR.size = love.physics.newRectangleShape(15, 515)
  staticObjects.goalBZR.fixture = love.physics.newFixture(goalBZR.body, goalBZR.size)
  staticObjects.goalBZR.fixture:setUserData("blastzone") 
end
  

  return staticObjects