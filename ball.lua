 ball = {}
 function ball.load()
  ball.body = love.physics.newBody(world, 450, 995, "dynamic")
  ball.body:setMass(30)
  ball.size = love.physics.newCircleShape(35)
  ball.fixture = love.physics.newFixture(ball.body, ball.size, 1)
  ball.fixture:setUserData("ball")
  ball.shots = {} -- holds our fired shots
 end
 
 function ball.draw()
  --let's draw the ball
  love.graphics.setColor(255,255,0,255)	
  love.graphics.circle("fill", ball.body:getX(),ball.body:getY(), ball.size:getRadius(), 20)
 end
 
 return ball