local blueCar = {} -- new table for the blueCar
  blueCar.body = love.physics.newBody(world, 300, 995, "dynamic")
  blueCar.size = love.physics.newRectangleShape(30, 15)
  blueCar.fixture = love.physics.newFixture(blueCar.body, blueCar.size)
  blueCar.fixture:setUserData("blueCar")
    
	blueCar.speed = 500
	blueCar.shots = {} -- holds our fired shots